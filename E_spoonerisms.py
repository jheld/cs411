import copy
import sys


vowels = {'a','e', 'i', 'o', 'u'}  # making 'y' special, and sets are hashed so O(1) lookup


def spoonerism(sentence):
    words = sentence.split(' ')
    a = words[0]
    b = words[-1]
    a_vowels = copy.copy(vowels)
    b_vowels = copy.copy(vowels)
    a_vowel_index = 0
    b_vowel_index = 0
    if a[0].lower() != 'y':
        a_vowels.add('y')
    else:
        a_vowels.discard('y')
    if b[0].lower() != 'y':
        b_vowels.add('y')
    else:
        b_vowels.discard('y')
    for idx, letter in enumerate(a):
        if letter.lower() in a_vowels:
            a_vowel_index = idx
            break
    for idx, letter in enumerate(b):
        if letter.lower() in b_vowels:
            b_vowel_index = idx
            break
    return ' '.join([b[:b_vowel_index] + a[a_vowel_index:]] + words[1:-1] + [a[:a_vowel_index] + b[b_vowel_index:]])


if __name__ == '__main__':
    # print(spoonerism('hello world'))
    # print(spoonerism('ease my tears'))
    # print(spoonerism('pack of lies'))
    # print(spoonerism('oiled bicycle'))
    # print(spoonerism('lighting a fire'))
    # print(spoonerism('dental receptionist'))
    # print(spoonerism('selling yaks'))
    # print(spoonerism('mystery house'))
    lines = sys.stdin.readlines()
    for line in lines:
        line = line.replace('\n', '')
        print(spoonerism(line))
