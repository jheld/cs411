from __future__ import unicode_literals, print_function

import sys


if __name__ == '__main__':
    output = []
    while True:
        line0 = sys.stdin.readline()
        line1 = sys.stdin.readline()
        if line0 != '' and line1 != '':
            output.append(str(int(line0) * int(line1)))
        else:
            break
    print('\n'.join(output))
