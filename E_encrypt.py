from __future__ import unicode_literals, division, print_function

from string import ascii_lowercase, ascii_uppercase
import sys

try:
    text_type = unicode
except Exception:
    text_type = str


syllables = {u'a': u'e', u'e': u'i', u'i': u'o', u'o': u'u', u'u': u'y', u'y': u'a',}
upper_syllables = {u'A': u'E', u'E': u'I', u'I': u'O', u'O': u'U', u'U': u'Y', u'Y': u'A',}


lower_consonants = sorted(set(ascii_lowercase) - set(syllables.keys()))
lower_num_consonants = len(lower_consonants)
lower_consonants_map = {k: lower_consonants[idx+1] if idx + 1 < lower_num_consonants else lower_consonants[0] for idx, k in enumerate(lower_consonants)}
upper_consonants = sorted(set(ascii_uppercase) - set(upper_syllables.keys()))
upper_num_consonants = len(upper_consonants)
upper_consonants_map = {k: upper_consonants[idx+1] if idx + 1 < upper_num_consonants else upper_consonants[0] for idx, k in enumerate(upper_consonants)}


def encrypt_data(source_data):
    for item in source_data:
        mapped = syllables.get(item, upper_syllables.get(item))
        if not mapped:
            mapped = lower_consonants_map.get(item, upper_consonants_map.get(item, item))
        yield str(mapped)


if __name__ == '__main__':
    data_input = sys.stdin.read()
    print(u''.join(encrypt_data(data_input)))
