import sys
from itertools import combinations


def slope(a, b):
    if b[0] - a[0] == 0:
        return None
    return (b[1] - a[1]) / float(b[0] - a[0])


def classifier(points):
    for item in combinations(points, 3):
        is_degenerate = (item[0][0] * (item[1][1] - item[2][1]) + item[1][0] * (item[2][1] - item[0][1]) + item[2][0] * (item[0][1] - item[1][1])) / 2
        if is_degenerate == 0:
            return 'degenerate'

    number_parallel = 0
    # 0, 1, 2, 3, yields 1-0,2-3, opposite is 3-0,2-1
    first_slope = slope(points[0], points[1])
    second_slope = slope(points[3], points[2])
    if first_slope == second_slope:
        number_parallel += 1
    first_slope = slope(points[0], points[3])
    second_slope = slope(points[1], points[2])
    if first_slope == second_slope:
        number_parallel += 1
    if number_parallel and number_parallel % 2 == 0:
        return 'parallelogram'
    elif number_parallel == 1:
        return 'trapezoid'
    return 'quadrilateral'


if __name__ == '__main__':
    # print(classifier([(0, 0), (0, 2), (2, 2), (2, 0)]))
    # print(classifier([(0, 0), (1, 2), (3, 2), (2, 0)]))
    # print(classifier([(1, 0), (1, 2), (2, 2), (3, 0)]))
    # print(classifier([(-2, -2), (2, 2), (6, -2), (2, -6)]))
    # print(classifier([(-2, -2), (2, 2), (6, -2), (2, -5)]))
    # print(classifier([(-1, -2), (0, 0), (1, 2), (2, 4)]))
    # print(classifier([(1, 2), (0, 0), (2, 4), (-1, -2)]))
    # print(classifier([(1, 2), (2, 4), (-1, -2), (0, 0)]))
    lines = sys.stdin.readlines()

    for idx, line in enumerate(lines):
        line = line.replace('\n', '')
        points = []
        point = []
        for i_index, item in enumerate(line.split(' ')):
            point.append(int(item))
            if i_index % 2 != 0:
                points.append(point)
                point = []
        print('FIGURE {} {}'.format(idx + 1, classifier(points)))
