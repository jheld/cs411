import sys
from collections import defaultdict
from string import ascii_lowercase


def get_positions_by_ascii(input_string):
    """
    Return dictionary of letters to their list of positions in the input
    :param input_string:
    :return:
    """
    positions_by_ascii = defaultdict(list)
    for idx, item in enumerate(input_string):
        if item.lower() in ascii_lowercase:
            positions_by_ascii[item.lower()].append(idx)
    return positions_by_ascii


def remove_letters(p_key, key_order, p_dict, input_string):
    """
    Removes letter at given p_key
    :param p_key:
    :param key_order:
    :param p_dict:
    :param input_string:
    :return:
    """
    indexes = p_dict[p_key]
    for other_key in key_order:
        other_value = p_dict[other_key]
        # as we remove this key, we must update the indexes of the other letter's indexes
        # such that if we're removing an index earlier than this one, that we must subtract 1 for each earlier removal
        if other_key != p_key:
            for o_v_index, o_v in enumerate(other_value):
                new_o_v = o_v - sum(1 for index in indexes if o_v > index)
                if new_o_v != o_v:
                    other_value[o_v_index] = new_o_v
    adjusted_string = ''.join(item for item_idx, item in enumerate(input_string) if item_idx not in indexes)
    return adjusted_string


def pairwise(t):
    """
    Given some t iterable, return pairs such that:
    [1, 2, 3, 4]
    (1, 2), (2, 3), (3, 4) are returned
    :param t:
    :return:
    """
    it = iter(t)
    second_value = None
    for _ in range(len(t)):
        if second_value is None:
            first_value = next(it)
        else:
            first_value = second_value
        second_value = next(it)
        yield first_value, second_value


if __name__ == '__main__':
    # data_lines = [
    #     'Ha, here comes an easy problem.',
    #     'Golly Gee Whiz',
    #     'Shalom Hello World',
    #     '   .',
    #     '.',
    # ]
    data_lines = sys.stdin.readlines()
    adjusted_lines = []
    for data_line in data_lines:
        data_line = data_line.replace('\n', '')
        position_dict = get_positions_by_ascii(data_line)
        sorted_keys = sorted(position_dict, key=lambda x: len(position_dict[x]), reverse=True)
        if len(sorted_keys) > 1:
            any_swap = True
            # not the most efficient sorting, but it should guarantee that everything with the same cardinality
            # will be in alphabetic order
            while any_swap:
                for i, j in pairwise(sorted_keys):
                    if j < i and len(position_dict[i]) == len(position_dict[j]):
                        i_index = sorted_keys.index(i)
                        sorted_keys[i_index] = j
                        sorted_keys[i_index + 1] = i
                        break
                else:
                    any_swap = False
        if sorted_keys:
            for key_idx, key in enumerate(sorted_keys):
                if key_idx + 1 == len(sorted_keys):
                    break
                data_line = remove_letters(key, sorted_keys[key_idx+1:], position_dict, data_line)
                position_dict.pop(key)
                adjusted_lines.append(data_line)
        # elif data_line != '.':  # restriction of the program, keep the input intact if not just '.'
        #     adjusted_lines.append(data_line)
    print('\n'.join(adjusted_lines))
    # test_output = [
    #     'Ha, hr coms an asy problm.',
    #     'H, hr coms n sy problm.',
    #     ', r coms n sy problm.',
    #     ', r cos n sy probl.',
    #     ', r cs n sy prbl.',
    #     ',  cs n sy pbl.',
    #     ',  c n y pbl.',
    #     ',  c n y pl.',
    #     ',   n y pl.',
    #     ',   n y p.',
    #     ',    y p.',
    #     ',    y .',
    #     'Golly G Whiz',
    #     'olly  Whiz',
    #     'oy  Whiz',
    #     'oy  Wiz',
    #     'oy  Wz',
    #     'y  Wz',
    #     'y  z',
    #     '  z',
    #     'Shaom Heo Word',
    #     'Sham He Wrd',
    #     'Sam e Wrd',
    #     'Sm e Wrd',
    #     'Sm e Wr',
    #     'Sm  Wr',
    #     'S  Wr',
    #     'S  W',
    #     '  W',
    #     '   .',
    # ]
    # assert adjusted_lines == test_output
