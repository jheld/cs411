from __future__ import unicode_literals, division, print_function

import sys

try:
    from itertools import zip_longest
except ImportError:
    from itertools import izip_longest as zip_longest

from string import ascii_lowercase, whitespace

try:
    text = unicode
except Exception:
    text = str

vowels = 'aeiouy'
consonants = (set(ascii_lowercase) - set(vowels)).union({'y'})


def authorship_driver(input_data):
    input_data = input_data.split('*')
    results = []
    for idx, d in enumerate(input_data, start=1):
        if d and d not in whitespace and d != ' ':
            hits, total = authorship_test(d)
            results.append('Case {idx}: {hits}/{total} = {ratio:.1f}%'.format(idx=idx,
                                                                              hits=hits,
                                                                              total=total,
                                                                              ratio=hits/total * 100))
    return results


def authorship_test(data):
    if isinstance(data, text):  # can take in a string and derive the data words
        data_list = []
        # given that the data length is limited to 80 characters, seems okay to just outright replace twice
        data = data.replace('\n', ' ')
        data = data.replace('\t', ' ')
        first_item, sep, remaining = data.partition(' ')
        while sep != '' and remaining != '':
            if first_item:
                data_list.append(first_item)
            first_item, sep, remaining = remaining.partition(' ')
        if first_item:
            # get the last data word when there is no further partition remaining
            data_list.append(first_item)
        data = data_list
        pattern, data = data[0], data[1:]
    else:
        pattern, data = data
    hit_count = 0
    total_words = len(data)
    for item in data:
        did_match = False
        for p, element in zip_longest(pattern, item, fillvalue=None):
            # if we have exhausted pattern, then that means it matched
            if not p:
                did_match = True
                break
            # if we have exhausted the item, then we bail-out early (did not match)
            elif not element:
                break
            if p == 'C':
                if element not in consonants:
                    break
            elif p == 'V':
                if element not in vowels:
                    break
            else:  # regular letter, so let's just check for equality
                if element != p:
                    break
        else:  # the pattern and item matched and were even length
            did_match = True
        if did_match:
            hit_count += 1
    return hit_count, total_words


if __name__ == '__main__':
    data_input = sys.stdin.read()
    test_output = authorship_driver(data_input)
    print('\n'.join(test_output))
