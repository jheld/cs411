from __future__ import unicode_literals, print_function
import argparse
import os
import sys
import traceback
from io import open
from itertools import groupby


def rle(input_data, include_newline=True):
    """

    :param input_data:
    :param include_newline:
    :return:
    """
    output_data = []
    for line in input_data:
        output_line = ''
        last_regular_sequence_idx = -1
        for idx, group in enumerate(groupby(line)):
            g_name, g_iter = group
            if g_name == '\n':
                continue
            g_list = list(g_iter)
            if len(g_list) > 1:  # repeated sequence
                if last_regular_sequence_idx != -1:
                    output_line += '1'
                    # we have consumed the previous regular sequence, so remove its index
                    last_regular_sequence_idx = -1
                output_line += '{num}{item}'.format(num=len(g_list), item=g_name)
            else:  # regular sequence
                # output the regular sequence start here if we don't have a previous one set
                if last_regular_sequence_idx == -1:
                    output_line += '1'
                output_line += g_name  # output the item itself
                if g_name == '1':
                    output_line += '1'
                # if we haven't yet seen a regular sequence, make sure its set by this idx
                if last_regular_sequence_idx == -1:
                    last_regular_sequence_idx = idx
        if last_regular_sequence_idx != -1:
            output_line += '1'
        output_data.append(output_line)
    output_data = '\n'.join(output_data)
    if include_newline:
        output_data += '\n'  # one last newline to match what was in the input
    return output_data


if __name__ == '__main__':
    data = sys.stdin.readlines()
    encoded = rle(data)
    print(encoded)
