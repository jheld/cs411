import sys


def binary_add(a, b, carries):
    if not a or not b:
        current = a or b
        if carries and carries[0]:
            if current and int(current[-1]):
                rec_ret = binary_add(a[:-1], b[:-1], [1] + carries)
                added, carries = rec_ret
                return added + '0', carries
            else:
                if current:
                    current = current[:-1] + '1'
                else:
                    current = '1'
                return current, carries
        else:
            return current, [''] + carries
    if int(a[-1]) == 1 and int(b[-1]) == 1:
        next_last = '0'
        if carries and carries[0]:
            next_last = '1'
        rec_ret = binary_add(a[:-1], b[:-1], [1] + carries)
        added, carries = rec_ret
        return added + next_last, carries
    else:
        if carries and carries[0]:
            if int(a[-1]) or int(b[-1]):
                rec_ret = binary_add(a[:-1], b[:-1], [1] + carries)
                added, carries = rec_ret
                return added + '0', carries
            else:
                rec_ret = binary_add(a[:-1], b[:-1], [''] + carries)
                added, carries = rec_ret
                return added + '1', carries
        else:
            rec_ret = binary_add(a[:-1], b[:-1], [''] + carries)
            added, carries = rec_ret
            return added + str(int(a[-1]) or int(b[-1])), carries

def binary_add2(a, b, carry):
    if a and b:
        if int(a[-1]) and int(b[-1]):
            rec_ret = binary_add2(a[:-1], b[:-1], 1)
            added, carries = rec_ret
            return rec_ret, [1] + [carry]


if __name__ == '__main__':
    input_data = sys.stdin.readlines()
    # input_data = ['001010100', '1101']
    # adds = [('11', '0101'), ('1010', '1011'), ('001010100', '01101')]
    adds = []
    adding = []
    for line in input_data:
        line = line.replace('\n', '').replace(' ', '')
        if line:
            adding.append(line)
        elif adding:
            adds.append(adding)
            adding = []
    if adding:
        adds.append(adding)
    for op_a, op_b in adds:
        ret = binary_add(op_a, op_b, [])
        # print(ret)
        line_len = max(len(op_a), len(op_b)) + 1
        print('BINARY SUM:')
        carries_here = [str(item) if item else ' ' for item in ret[-1]]
        carries_here += ' '  # one at the end because we never have a begin carry
        if len(carries_here) > line_len:
            line_str = ''.join(carries_here[len(carries_here) - line_len:])
        else:
            line_str = ' ' * (line_len - len(carries_here)) + ''.join(carries_here)
        print(line_str)
        line_str = op_a
        line_str = (' ' * (line_len - len(line_str))) + line_str
        print(line_str)
        line_str = op_b
        line_str = (' ' * (line_len - len(line_str))) + line_str
        print(line_str)
        print('-' * line_len)
        line_str = ret[0]
        line_str = ('0' * (line_len - len(line_str))) + line_str
        print(line_str)
        print('')


