from __future__ import unicode_literals, print_function

import sys
import string
from collections import Counter


def special_huffman(t_counter):
    if len(tree_counter) == 1:
        return list(t_counter.keys())[0]
    smallest_order = sorted(tree_counter.items(), key=lambda x: x[1])
    s0, s1 = smallest_order[:2]
    combined_value = s0[1] + s1[1]
    t_counter.pop(s0[0])
    t_counter.pop(s1[0])
    first_s0 = next(i for i in s0[0] if i in string.ascii_lowercase)
    first_s1 = next(i for i in s1[0] if i in string.ascii_lowercase)
    if first_s0 <= first_s1:
        new_key = '({})({})'.format(s0[0], s1[0])
    else:
        new_key = '({})({})'.format(s1[0], s0[0])
    t_counter[new_key] = combined_value
    return special_huffman(t_counter)


if __name__ == '__main__':
    # input_str = 'abaacb'
    # tree_counter = Counter()
    # for item in input_str:
    #     tree_counter[item] += 1
    # output = special_huffman(tree_counter)
    # print(output)
    # input_str = 'abbcccddddeeeeeffffffggggggghhhhhhhh'
    # tree_counter = Counter()
    # for item in input_str:
    #     tree_counter[item] += 1
    # output = special_huffman(tree_counter)
    # print(output)

    all_input = sys.stdin.readlines()
    for idx, line in enumerate(all_input):
        tree_counter = Counter()
        for item in line.replace('\n', ''):
            tree_counter[item] += 1
        output = special_huffman(tree_counter)
        print('String {}: {}'.format(idx + 1, output))
