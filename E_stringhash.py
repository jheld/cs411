import sys


def string_hash(obj, m, p):
    """

    :param obj:
    :param m:
    :param p:
    :return:
    """
    if obj:
        return ((p * string_hash(obj[:-1], m, p)) + ord(obj[-1])) % m
    else:
        return 0

if __name__ == '__main__':
    # print(string_hash('AB', 10000, 100))
    # print(string_hash('AB', 32000, 33))
    # print(string_hash('ABCDEFGHIJ', 32000, 33))
    # print(string_hash('BACDEFGHIJ', 32000, 33))
    # print(string_hash('%^@abc++=903#?..."', 32000, 33))
    lines = sys.stdin.readlines()
    # lines = ['10000 100 AB', ]
    for line in lines:
        line = line.replace('\n', '')
        line_buff = ''
        line_m = ''
        line_p = ''
        line_str = ''
        found_line_m = False
        found_line_p = False
        for item in line:
            if not line_m:
                line_m += item
            elif item.isdigit():
                if not line_p and not found_line_m:
                    line_m += item
                elif not found_line_p:
                    line_p += item
            else:
                if line_m and not line_p:
                    found_line_m = True
                elif line_p:
                    found_line_p = True
            if found_line_m and found_line_p and item not in ' \t':
                line_str += item
        print('{} {} {} {}'.format(line_m, line_p, line_str, string_hash(line_str, int(line_m), int(line_p))))
